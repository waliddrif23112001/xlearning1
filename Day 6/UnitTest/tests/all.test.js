const tab = require("../all");
// First Method
it("Length table ", () => {
  expect(tab.length).toBe(7);
});

// Second Method
it("Length table ", () => {
  expect(tab).toHaveLength(7);
});

test("Contain num 2 ", () => {
  expect(tab).toContain(2);
});

it("Not Contain num 2 ", () => {
  expect(tab).not.toContain(444);
});

it("Not Contain num 33 ", () => {
  for (let i = 0; i < tab.length; i++) {
    expect(tab[i]).not.toBe(33);
  }
});

it("if array contain only numbers 1st method", () => {
    for (let i = 0; i < tab.length; i++) {
      expect(isNaN(tab[i])).toBe(false);
    }
  });
it("if array contain only numbers 2nd method", () => {
    for (let i = 0; i < tab.length; i++) {
      expect(isNaN(tab[i])).toBeFalsy();
    }
  });
it("if array contain only numbers 3th method", () => {
    for (let i = 0; i < tab.length; i++) {
      expect(isNaN(tab[i])).not.toBeTruthy();
    }
  });

it("if the first element larger than or equal to 1 ", () => {
    expect(tab[0]).toBeGreaterThanOrEqual(1)
});

it("if the second element less than 2 ", () => {
    expect(tab[1]).toBeLessThan(2)
});

it("Check for undefind", () => {
    let a
    expect(a).toBeUndefined()
});

it("Check for subString inside String var", () => {
    let str="walid Drif"
    expect(str).toMatch(/walid/)
});

it("Check for propety", () => {
    let obj={
            name:'Walid',
            age:21
    }
    expect(obj).toHaveProperty('name')
});

it("Check for propety name has value Walid", () => {
    let obj={
            name:'Walid',
            age:21
    }
    expect(obj).toHaveProperty('name','Walid')
});


expect.extend({
    toBeGr(received,target){
        const pass =received>target
        if (pass) {
            return{
                message:()=>`Expected ${received} greater than ${target}`,
                pass:true
            }
        } 
        else {
            return {
                message:()=>`Error : Expected ${received} greater than ${target}`,
                pass:false
            }
        }
    }
})

it("check number",()=>{
    expect(13).toBeGr(12)
})

expect.extend({
    toBeBetween(received,rangeStart,rangeEnd){
        const pass=received>rangeStart && received<rangeEnd
        if (pass) {
            return{
                message:()=>`Expected ${received} between ${rangeStart} and ${rangeEnd}`,
                pass:true
            }
        } else {
            return{
                message:()=>`Error: Expected ${received} not between ${rangeStart} and ${rangeEnd}`,
                pass:false
            }
        }
    }
})

it("check if the num is between 10 and 15",()=>{
    expect(13).toBeBetween(10,15)
})
