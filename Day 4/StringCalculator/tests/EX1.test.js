const StringCalculator = require("../EX1")


sc=new StringCalculator();

describe("StringCalculator",()=>{
    test('if the arg is "" ', () => {
      expect(sc.Add("")).toBe(0)
    })
    test('if the arg is "1" ', () => {
        expect(sc.Add("1")).toBe(1)
      })
      test('if the arg is "1,2" ', () => {
        expect(sc.Add("1,2")).toBe(3)
      })
})