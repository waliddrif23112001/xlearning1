class StringCalculator{
    Add=(arg)=>{
        if (arg == "") {
            return 0
        }
        
        if (!arg.includes(",")) {
                return Number(arg)
        }
        let tab=arg.split(",")
        return tab.reduce((pv,cv)=>Number(pv)+Number(cv))      
    }
}
module.exports=StringCalculator;