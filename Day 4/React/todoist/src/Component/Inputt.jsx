import React, { useState } from 'react'
import ListElement from './ListElement'


function Inputt() {
    const [word,setWord]=useState("")
    const [list,setList]=useState([])
    const valid=(mot)=>{
      setList([...list,mot])
    }
  return (
    <div className='mt-3' >
      <h1 className="text-success text-center">TODO LIST</h1>
    <div className='d-flex row mt-3'>
      <div className='col-9 '>
      <input type="text"
      className='form-control shadow-none' 
      onChange={e=>{
          setWord(e.target.value)
      }} />
      </div>
      <div className='col-3'>
      <button type='button' className='btn btn-success shadow-none' onClick={()=>{valid(word)}}>OK</button>
      </div>
    </div>

    <div >
    <ul class="list-group mt-2 border border-success">

    {list.map((e,i)=>{
       return <ListElement element={e} key={i}/>
      })}

      </ul>
      </div>

    </div>
  )
}

export default Inputt
