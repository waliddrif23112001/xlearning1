import React from 'react'

function ListElement({element}) {
  return (
    <li class="list-group-item border border-success">{element}</li>
  )
}

export default ListElement
