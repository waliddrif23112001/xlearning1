// import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import { Button,Input,Select,Form,Table,message, Alert,DatePicker
    ,TimePicker
} from 'antd'
import { PoweroffOutlined,UserOutlined } from '@ant-design/icons'
import { useState } from 'react';
import Password from 'antd/lib/input/Password';


function App() {
  const [load,setLoad]=useState(false)
  const [showAlert,setshowAlert]=useState(false)
  const click=_=>{
    // setLoad(true)
    setshowAlert(false)
    setTimeout(()=>{
      // setLoad(false)
      // message.success("Very good")
      setshowAlert(true)
    }
    ,2000)
  }
  const fruits=['Kiwi','Apple','Orange']
  const data=[{
      name:'walid',
      age:21,
      city:'Guelmim'
    },{
      name:'yasser',
      age:18,
      city:'Agadir'
    },{
      name:'said',
      age:21,
      city:'Casablanca'
    }]
    const columns=[{
      title:'Name',
      dataIndex:'name',
      key:'key',
      render:name=>{
        return <a>{name}</a>
      }
    },
    {
      title:'Age',
      dataIndex:'age',
      key:'key',
      sorter:(a,b)=>a.age-b.age
    },{
      title:'City',
      dataIndex:'city',
      key:'key'
    },{
      title:'Graduated',
      key:'key',
      render:payload=>{
        return <p>{payload.age>20?'true':'false'}</p>
      }
    }
  ]

  return (
    <div className="App">
      <header className="App-header">
        {/* {showAlert&&<Alert type='success'
        message='Error'
        description='This is Antd alert'
        closable
        
        />}
        <div style={{"width":'50%'}}>
        <Button 
        loading={load}
        style={{"color":'red','backgroundColor':'orange'}}
        block 
        icon={<PoweroffOutlined/>}
        type='default'
        onClick={click}
        >First button</Button></div> */}
        {/* <Input.Search 
        placeholder='Enter Here...'
        maxLength={5}
        type='text'
        prefix={<UserOutlined/>}
        allowClear
        /> */}
        {/* <Select 
        mode='multiple' 
        maxTagCount={2}
       
        placeholder="select Fruit" 
        style={{'width':'50%'}}>
          {fruits.map((fruit,index)=>{
            return <Select.Option key={index} value={fruit}>{fruit}</Select.Option>
          })}
        </Select> */}
        {/* <Form >
          <Form.Item label='User Name' name='UserName'>
          <Input 
          maxLength={5}
          allowClear
          required
          placeholder='Enter Name...'/>
          </Form.Item>
          <Form.Item label='Password' name='Password'>
          <Input.Password
          type='Password'
          allowClear
          required
          placeholder='Enter Password...'/>
          </Form.Item>
          <Form.Item >
          <Button block htmlType='submit' type='primary'>Submit</Button>
          </Form.Item>
        </Form> */}
        {/* <Table
        dataSource={data}
        columns={columns}
        
        >

        </Table> */}
        <div>
          <DatePicker picker='month' />
          <DatePicker.RangePicker />
          <TimePicker />
        </div>

      </header>
    </div>
  );
}

export default App;
