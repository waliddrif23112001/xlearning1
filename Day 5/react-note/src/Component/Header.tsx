import * as React from 'react';
import { Container, Navbar, NavbarBrand } from 'react-bootstrap';

interface IHeaderProps {
}

const Header: React.FunctionComponent<IHeaderProps> = (props) => {
  return (
    <Navbar fixed='top' bg="success" variant='dark' expand="lg">
      <Container>
        <NavbarBrand>
          MY NOTE
        </NavbarBrand>
      </Container>
    </Navbar>
  );
};

export default Header;

