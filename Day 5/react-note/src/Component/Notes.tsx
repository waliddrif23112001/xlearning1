import * as React from 'react';
import { Button, Card } from 'react-bootstrap';
import { Note } from '../Models/Note.model';

interface INotesProps {
  data:Note,
  handelDelete:(id:string)=>void
}

const Notes: React.FunctionComponent<INotesProps> = ({data,handelDelete}) => {
  const {id,title,description,color,date}=data;
  return (
    <div className='mt-3'>
      <Card style={{backgroundColor:color }} className='text-light'>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle className='text-dark'>{date}</Card.Subtitle>
        <Button className=' mt-3 shadow-none' variant='outline-danger'
        onClick={()=>handelDelete(id)}
        >Delete</Button>
      </Card.Body>
    </Card>
    </div>
  )
};

export default Notes;
