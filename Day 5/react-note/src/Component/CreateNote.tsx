import * as React from 'react';
import { Alert, Button, Form } from 'react-bootstrap';
import { Note } from '../Models/Note.model';

interface ICreateNoteProps {
  note:Note[],
  setNotes:React.Dispatch<React.SetStateAction<Note[]>>
}

const CreateNote: React.FunctionComponent<ICreateNoteProps> = ({note,setNotes}) => {
  const NoteTitle=React.useRef<HTMLInputElement | null>(null);
  const NoteDesc=React.useRef<HTMLTextAreaElement | null>(null);
  const NoteColor=React.useRef<HTMLInputElement | null>(null);
  const [error,setError]=React.useState<string>("")

  const handelSubmit=(e:React.FormEvent<HTMLFormElement>):void=>{
      e.preventDefault();
      if (NoteTitle.current?.value==="" || NoteDesc.current?.value==="") {
        return setError("You Forgot to enter all informations")
      }
      setNotes([...note,{
        id:(new Date()).toISOString(),
        title:(NoteTitle.current as HTMLInputElement).value,
        description:(NoteDesc.current as HTMLTextAreaElement).value,
        color:(NoteColor.current as HTMLInputElement).value,
        date:(new Date()).toUTCString().toString()
      }]);
      (NoteTitle.current as HTMLInputElement).value ="";
      (NoteDesc.current as HTMLTextAreaElement).value ="";
      setError("")
  }
  
  return (
    <div className='mt-3'>
        <h2>Create Notes</h2>
        {error && <Alert variant='danger'>{error}</Alert>}
        <Form className='my-3 border border-success p-3' 
        onSubmit={(e)=>{handelSubmit(e);}}
        >
        <Form.Group className='mt-3'>
            <Form.Label>Title</Form.Label>
            <Form.Control type='text' placeholder='Enter Title Note...' ref={NoteTitle} />
          </Form.Group>
          <Form.Group className='mt-3'>
            <Form.Label>Description</Form.Label>
            <Form.Control type='text' as='textarea' rows={3} 
            placeholder='Enter Description Note...' ref={NoteDesc}/>
          </Form.Group>
          <Form.Group className='mt-3'>
            <Form.Label>Note Color</Form.Label>
            <Form.Control type='color' defaultValue="#ccc" id='noteColor' ref={NoteColor}/>
          </Form.Group>
          <Button variant='success' type='submit' className='shadow-none mt-2'>Add Note</Button>
        </Form>
    </div>
  )
};

export default CreateNote;

