import * as React from 'react';
import { Note } from '../Models/Note.model';
import Notes from './Notes';


interface INoteListProps {
  note:Note[],
  setNotes:React.Dispatch<React.SetStateAction<Note[]>>
}

const NoteList: React.FC<INoteListProps> = ({note,setNotes}) => {
    const handelDelete=(id:string)=>{
      setNotes(note.filter(Notes=>Notes.id !== id))
    }
  const dsd=():JSX.Element[]=>{
    return note.map(e=>
      {return <Notes key={e.id} data={e} handelDelete={handelDelete}/>})
    
  }
  return (
    <>
        <h2 className='mt-3'>Notes</h2>
        <div>{ dsd() }</div>
    </>
  )
};

export default NoteList;

