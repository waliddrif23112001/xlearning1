import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Note } from "./Models/Note.model";
import Header from "./Component/Header";
import NoteList from "./Component/NoteList";
import CreateNote from "./Component/CreateNote";


function App() {
  const [Notes, setNotes] = useState<Note[]>([
    {
      id: "2",
      title: "walid",
      description: "dfldflfdklfkldfldklfkdlfkdl",
      color: "violet",
      date:(new Date()).toString(),
    },
    {
      id: "3",
      title: "walid",
      description: "dfldflfdklfkldfldklfkdlfkdl",
      color: "lime",
      date: "3-10-2022",
    }
  ]);
  return (
    <>
    <Header/>
    <Container className="mt-5">
      <Row>
        <Col>
        <NoteList note={Notes} setNotes={setNotes}/>
        </Col>
      </Row>
      <Row>
        <Col>
        <CreateNote note={Notes} setNotes={setNotes}/>
        </Col>
      </Row>
    </Container>
    </>
  );
}

export default App;
