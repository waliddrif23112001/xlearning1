export interface Note {
    id: string;
    title: string;
    description: string;
    color: string;
    date: string;
  }