const tests=require("../Tests")

//expect.anything() but not indefind or null
test('Expect anything ', () => {
  expect("walid").toEqual(expect.anything())
  expect([3,"dd",4,5454,"ff"]).toEqual(expect.anything())
})

// expect.any(Constructor)
test('Expect any ', () => {
  expect(33).toEqual(expect.any(Number))
})

// expect.arrayContaining(array)
test('expect the array elements to be in the main array', () => {
    let tab=[1,5,6,3]
  expect(tab).toEqual(expect.arrayContaining([1,5,5,1,6,6,6,6]))
})




