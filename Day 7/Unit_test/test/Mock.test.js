const mocker=require("../Mock")


test('Mock Function ', () => {
    const mocker=jest.fn(name=>`Hello ${name}`)
    expect(mocker("walid")).toBe("Hello walid")
    expect(mocker("said")).toBe("Hello said")
    expect(mocker).toHaveBeenCalled()
    expect(mocker).toHaveBeenCalledTimes(2)
    expect(mocker).toHaveBeenCalledWith("walid")
    expect(mocker).toHaveBeenLastCalledWith("said")
})
