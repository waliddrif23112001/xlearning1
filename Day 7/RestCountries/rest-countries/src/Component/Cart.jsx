import React from 'react'
import { Card } from 'antd'
import Meta from 'antd/lib/card/Meta'
import { Link } from 'react-router-dom'

function Cart({country,id}) {

    console.log('kjdkd',id)
  return (
    <Link to={`/CountryDetail/${id}`}>
    <Card
    hoverable
    style={{ width: 240 ,height:300}}
    className="my-10 border border-r-emerald-900  "
    cover={<img alt={country.cca2} src={country.flags.svg}  />}
    
        >
    <Meta title={country.name.common} description={country.capital} />
  </Card>
  </Link>

  )
}

export default Cart
