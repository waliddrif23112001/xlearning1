import React from 'react'
import { useContext } from 'react'
import Cart from '../Component/Cart'
import { dataContext } from '../Context/dataContext'

function Home() {
    const data=useContext(dataContext)
   
  return (
    <div className=' container mx-auto '>
        <div className='w-full '>
            <h1 className='text-4xl'>Home</h1>
        </div>

        

        <div className=' lg:columns-4 md:columns-3 sm:columns-1'>
             {
           (data) ? data.map((e,key)=>{
            // console.log(key)
            return  <Cart country={e} key={key} id={key}/>
            }):
            <h1 className='text-4xl font-bold text-red-600 items-center'>Loading...</h1>
            }
        </div>
       
       
        
    </div>
  )
}

export default Home
