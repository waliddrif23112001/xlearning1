import Meta from 'antd/lib/card/Meta'
import React, { useContext } from 'react'
import { useParams } from 'react-router-dom'
import { dataContext } from '../Context/dataContext'

function CountryDetails() {
    const data=useContext(dataContext)
    const {id}=useParams()
    const {name,flags,cca2,capital}=data[id]
  return (
    <div>
     <div>
       <h1 className='text-4xl text-red-500 font-bold'>Detail Country</h1>
    </div>
    <div >
        <img className='w-full h-44' src={flags.svg} alt={cca2} />
        <Meta className='text-center font-bold' title={name.common} description={capital} />
    </div> 
    </div>
  )
}

export default CountryDetails
