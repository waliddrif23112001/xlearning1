
// import {useQuery} from 'react-query'
import axios from 'axios'
import {  useEffect, useState } from 'react'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { dataContext } from './Context/dataContext';
import CountryDetails from './Pages/CountryDetails';
import Home from './Pages/Home';
import NoPage from './Pages/NoPage';


function App() {
  const [data,setData]=useState(null);
  useEffect(()=>{
    const fetchData=async()=>
    {try {
      const {data}=await axios("https://restcountries.com/v3.1/all")
      setData(data)
    } catch (error) {
      alert(error.message)
    }}
    fetchData()
  },[])

  
  return (
    <dataContext.Provider value={data}>
    <BrowserRouter>
    <Routes>
       <Route path="/" element={<Home />} />
       <Route path="/CountryDetail/:id" element={<CountryDetails />} />
        <Route path="*" element={<NoPage />} />
    </Routes>
  </BrowserRouter>
  </dataContext.Provider>
  );
}

export default App;
