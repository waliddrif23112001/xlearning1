function repeatStringNumTimes(str, num) {
  var i=0
  var result=""
  while(i<num){
    result+=str
    i++;
  }
  return result;
}

console.log(repeatStringNumTimes("abc", 4));