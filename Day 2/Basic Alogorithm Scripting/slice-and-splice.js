function frankenSplice(arr1, arr2, n) {
  let res=[]
  res.push(...arr2.slice(0,n))
  res.push(...arr1)
  res.push(...arr2.slice(n,arr2.length))
  
  return res;
}

console.log(frankenSplice(["claw", "tentacle"], ["head", "shoulders", "knees", "toes"], 2));