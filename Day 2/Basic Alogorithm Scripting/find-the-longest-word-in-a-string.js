function findLongestWordLength(str) {
  var tab=str.split(" ")
  var longWord=""
  for(var i=0;i<tab.length;i++){
    var word=tab[i]
    if(word.length>longWord.length){
      longWord=tab[i]
    }
  }
  
  return longWord.length;
}

console.log(findLongestWordLength("The quick brown fox jumped over the lazy dog"));