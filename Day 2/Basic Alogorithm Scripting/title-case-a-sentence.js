function titleCase(str) {
  var tab=[]
  var res=""
  tab=str.split(" ")
  for(var i=0;i<tab.length;i++){
    
    var word=tab[i]
    for(var j=0;j<word.length;j++){
      if(j==0){
        res+=word[j].toUpperCase()
      }
      else{
        res+=word[j].toLowerCase()
        }
    }
    if(i<tab.length-1) {
      res+=" "
    }
    
  }
  return res;
}

console.log(titleCase("I'm a little tea pot"));