
//create-a-basic-javascript-object
let dog = {
  name :"www",
  numLegs :3
};

//create-a-method-on-an-object
let dog1 = {
  name: "Spot",
  numLegs: 4,
  sayLegs:function(){return "This dog has 4 legs."}
};

dog.sayLegs();


//define-a-constructor-function
function Dog(){
  this.name="ww";
  this.color="red";
  this.numLegs=3 ;
}


// extend-constructors-to-receive-arguments
function Dog(name,color) {
  this.numLegs =4;
  this.name =name;
  this.color =color;
}
let terrier=new Dog();
terrier.name="ww";
terrier.color="blue";

//make-code-more-reusable-with-the-this-keyword
let dog2 = {
  name: "Spot",
  numLegs: 4,
  sayLegs: function() {return "This dog has " + this.numLegs + " legs.";}
};

dog.sayLegs();

//understand-own-properties
function Bird(name) {
  this.name = name;
  this.numLegs = 2;
}

let canary = new Bird("Tweety");
let ownProps = [];

for (let property in canary) {
  if(canary.hasOwnProperty(property)) {
    ownProps.push(property);
  }
}

console.log(ownProps);//return ["name", "numLegs"]

// use-a-constructor-to-create-objects
function Dog() {
  this.name = "Rupert";
  this.color = "brown";
  this.numLegs = 4;
}
let hound=new Dog();


// use-dot-notation-to-access-the-properties-of-an-object
let dog3 = {
  name: "Spot",
  numLegs: 4
};
console.log(dog.name);
console.log(dog.numLegs);

// use-prototype-properties-to-reduce-duplicate-code
function Dog(name) {
  this.name = name;
}

Dog.prototype.numLegs =13
let beagle = new Dog("Snoopy");
console.log(beagle.numLegs )//return 13


// verify-an-objects-constructor-with-instanceof
function House(numBedrooms) {
  this.numBedrooms = numBedrooms;
}
let myHouse =new House(4444)
myHouse instanceof House