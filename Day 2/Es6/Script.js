//JavaScript let
var x = 10;
//  x is 10
{
  let x = 2;
  //  x is 2
}
//  x is 10


//Arrow Functions
// ES5
var x = function(x, y) {
    return x * y;
 }
 
 // ES6
 const x = (x, y) => x * y;


//Spread Operator 
// ES5
var arr = [1, 2, 3];
var arr2 = [4, 5, 6];
var arr3 = arr.concat(arr2);
// ES6
var arr3 = [...arr, ...arr2];


//Rest Parameters 
// ES5
function f(x, y, z) {
    return x + y + z;
}
// ES6
function f(...args) {
    return args[0] + args[1] + args[2];
}


//Destructuring 
// ES5
var arr = [1, 2, 3];
var x = arr[0];
var y = arr[1];
var z = arr[2];
// ES6
var [x, y, z] = arr;


//Template Literals 
// ES5
var name = "John";
var age = 35;
var sentence = "His name is " + name + ". He is " + age + " years old.";
// ES6
var sentence = `His name is ${name}. He is ${age} years old.`;


//Classes 
// ES5
function Person(name, age) {
    this.name = name;
    this.age = age;
}
Person.prototype.sayName = function() {
    console.log(this.name);
}
// ES6
class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    sayName() {
        console.log(this.name);
    }
}


//Import from named exports 
import { name, age } from "./file.js";

//Import from default exports
import name from "./file.js";
