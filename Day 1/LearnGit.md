


git is a version control system that is used to track changes in files and to keep track of the history of changes.
git is a free and open source software project that is developed by Linus Torvalds.
1-configure git :
- git config --global user.name 
- git config --global user.email
2-create a new branch :
- git branch "branchName"   
3-show branches : 
- git branch
4-change branch :
- git checkout "branchName"
5-show status :   
- git status
6-add file :          
- git add "File"
7-commit :    
- git commit -m "comentaire"

[]: # Language: markdown
[]: # Path: Day 1\LearnGit.md

