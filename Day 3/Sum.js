const sum=(...args)=>{
    return args.reduce((pv,cv)=>pv+cv,0)
}

module.exports=sum;