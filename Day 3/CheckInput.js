const CKName=name=>{
    if (name===undefined) {
        return "UnKnown";
    }
    if (name.startsWith(" ") || name.endsWith(" ")) {
        return name.trim()
    }
    if (name.length >10) {
        return name.substring(0,10)
    }
    if (name.startsWith("_")) {
        return name.substring(1)
    }
    return name
}

module.exports=CKName;