const sum = require("../Sum");

/*
test("name",fn,timeout)
it("name",fn,timeout)
describe("name",fn)
*/


describe("check Sum of numbers", () => {
  describe("for no number or one ", () => {
    it(" return 0 if no number", () => {
      expect(sum()).toBe(0);
    });
    it(" number", () => {
      expect(sum(10)).toBe(10);
    });
  });
  describe("For more than one number", () => {
    it("n1 + n2", () => {
      expect(sum(10, 20)).toBe(30);
    });

    it("n1 + n2 + n3", () => {
      expect(sum(10, 20, 20)).toBe(50);
    });
    it("Sum of all numbers", () => {
      expect(sum(30, 40, 30, 100, 300, 5)).toBe(505);
    });
  });
});
