const CKN=require('../CheckInput')

describe("Check Name ",()=>{

    it("No name Entered",()=>{
        expect(CKN()).toBe("UnKnown")
    })
    it("Check space ",()=>{
        expect(CKN("   walid  ")).toBe("walid")
    })
    it("Check name length",()=>{
        expect(CKN("walid_drif_www")).toBe("walid_drif")
    })
    it("check name don't start with underscore",()=>{
        expect(CKN("_walid")).toBe('walid')
    })
})